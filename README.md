# README #

This theme framework was made for speeding up the process of theme development for wordpress websites.

## Synopsis

The theme framework is made in Twitter Bootstrap for UI helpers, using Webpack to manage front end tasks.

## Installation

Please follow the steps to clone in local computer from git repo.
```sh
npm install
```

## Run the application

To run the application locally, use
```sh
npm run start
```
